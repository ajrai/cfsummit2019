<cfpresentation 
	title="Sales Presentation" 
	destination="#ExpandPath('myppt.ppt')#" 
	format="ppt" <!--- format="html" will create HTML version of presentation --->
	overwrite="yes" >
	
<cfpresenter name="Kailash" title="Senior Software Eng" email="bihani@adobe.com"> 
<cfpresenter name="Mayur" title="Computer Scientist 2" email="mjain@adobe.com"> 

<cfpresentationslide 
	src="#ExpandPath('aboutMe.html')#" 
	title="Introduction" 
	presenter="Kailash"
	/> 
 
<cfpresentationslide> 
	<h3>Sales</h3> 
	<ul> 
		<li>Overview</li> 
		<li>Q1 Sales Figures</li>
		<li>Projected Sales</li> 
		<li>Competition</li> 
		<li>Advantages</li> 
		<li>Long Term Growth</li> 
	</ul> 
</cfpresentationslide>

<cfpresentationslide> 
	<cfchart format="jpg" type="bar" showlegend="false" chartHeight="400" chartWidth="600" title="Basic Chart" show3d="true">
	    <cfchartseries>
	        <cfchartdata item="2015" value=20>
	        <cfchartdata item="2016" value=40>
	        <cfchartdata item="2017" value=60>
	    </cfchartseries>
	</cfchart>
</cfpresentationslide>

<cfpresentationslide>
	<h3>Pricing Information</h3> 
	<p>
		<table width="100%">
			<tr>
				<td><b>Product</b></td>
				<td><b>Price</b></td>
			</tr>
			<tr>
				<td>Apples</td>
				<td>$0.99</td>
			</tr>
			<tr>
				<td>Bananas</td>
				<td>$1.99</td>
			</tr>
			<tr>
				<td>Nukes</td>
				<td>$2.99</td>
			</tr>
		</table>
	</p>
</cfpresentationslide>
</cfpresentation>
