<cfquery name="courses" datasource="ecommerce"> 
       SELECT COURSE_NUMBER,DEPT_ID,COURSE_ID,COURSE_NAME 
       FROM COURSELIST 
</cfquery> 

<cfquery name="centers" datasource="ecommerce"> 
       SELECT * FROM CENTERS
</cfquery> 

<cfscript> 
    //Use an absolute path for the files. 
    theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
    theFile=theDir & "courses1.xls"; 
    //Create two empty ColdFusion spreadsheet objects. 
    theSheet = SpreadsheetNew("CourseData"); 
    theSecondSheet = SpreadsheetNew("CentersData");
    //Populate each object with a query.
    SpreadsheetAddRows(theSheet, courses); 
    SpreadsheetAddRows(theSecondSheet, centers); 
</cfscript>

<!--- Write the two sheets to a single file --->
<cfspreadsheet  
		action="write" 
		filename="#theFile#" 
		name="theSheet"
    	sheetname="courses" 
    	overwrite=true>
    		 
<cfspreadsheet  
		action="update" 
		filename="#theFile#" 
		name="theSecondSheet"
    	sheetname="centers">

