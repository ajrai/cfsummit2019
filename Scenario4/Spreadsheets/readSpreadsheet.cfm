<cfscript> 
    //Use an absolute path for the files.
    theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
    theFile=theDir & "courses.xls"; 
</cfscript>

<!--- Read all or part of the file into a spreadsheet object, CSV string, 
      HTML string, and query. --->
<cfspreadsheet action="read" src="#theFile#" sheetname="courses" name="spreadsheetData">
<cfspreadsheet action="read" src="#theFile#" sheet=1 rows="3,4" format="csv" name="csvData"> 
<cfspreadsheet action="read" src="#theFile#" format="html" rows="5-10" name="htmlData"> 
<cfspreadsheet action="read" src="#theFile#" sheetname="centers" query="queryData"> 

<h3>Spreadsheet data in cfspreadsheet object</h3>
<cfdump var="#spreadsheetData#">

<h3>First sheet row 3 and 4 read as a CSV variable</h3> 
<cfdump var="#csvData#"> 
   
<h3>Second sheet rows 5-10 read as an HTML variable</h3> 
<cfdump var="#htmlData#">
   
<h3>Second sheet read as a query variable</h3> 
<cfdump var="#queryData#"> 
