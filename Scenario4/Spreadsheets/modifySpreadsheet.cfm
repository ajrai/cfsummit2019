<cfscript> 
    //Use an absolute path for the files. ---> 
    theDir=GetDirectoryFromPath(GetCurrentTemplatePath()); 
    theFile=theDir & "courses1.xls"; 
</cfscript>

<cfspreadsheet action="read" src="#theFile#" sheetname="courses" name="spreadsheetData">
<cfspreadsheet action="read" src="#theFile#" sheet=1 rows="3,4" format="csv" name="csvData">

<!--- Modify the courses sheet. --->
<cfscript> 
    SpreadsheetAddRow(spreadsheetData,"10,123,1156,Poetry",10,1); 
    SpreadsheetAddColumn(spreadsheetData, 
    "Basic,Intermediate,Advanced,Basic,Intermediate,Advanced,Basic,Intermediate,Advanced", 
    3,2,true); 
</cfscript> 
   
<!--- Write the updated Courses sheet to a new XLS file --->
<cfspreadsheet action="write" filename="#theDir#updatedFile.xls" name="spreadsheetData"
    sheetname="courses" overwrite=true> 
<!--- Write an XLS file containing the data in the CSV variable. --->    
<cfspreadsheet action="write" filename="#theDir#dataFromCSV.xls" name="CSVData"
    format="csv" sheetname="courses" overwrite=true>

<cfdump var="#csvData#">
