<!--- EXAMPLE FOR SIMPLE PDF FORM --->
<!--- EXAMPLE FOR W2 FORM --->

<!--- READ XML IN STRUCTURE --->
<!---
<cfpdfform source="w2.pdf" result="resultStruct" action="read"/>
<cfdump var="#resultStruct#">
--->
<!--- READ IN XML --->
<!---
<cfset resultfile = Expandpath('w2.xml')> 
<cfpdfform source="w2.pdf" action="read" xmldata="xmldata"/>
<cffile action="write" file="#resultfile#" output="#xmldata#" >
<cfdump var="#xmldata#">
--->

<!--- POPULATE PDF FORM USING XML --->

<cfpdfform source="w2.pdf" 
			destination="w2out.pdf" 
			action="populate" 
			XMLdata="w2_filled.xml"
			overwrite="yes" />
