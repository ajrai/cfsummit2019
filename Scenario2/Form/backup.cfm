<!--- READ PDF FORM IN STRUCTURE --->
<cfpdfform source="PdfFormExample.pdf" result="resultStruct" action="read"/>
<cfdump var="#resultStruct#">

<!--- READ PDF FORM IN XML --->
<!---
<cfset resultfile = Expandpath('PdfFormExampleOutPut.xml')> 
<cfpdfform source="PdfFormExample.pdf" action="read" xmldata="xmldata"/>
<cffile action="write" file="#resultfile#" output="#xmldata#" >
<cfdump var="#xmldata#">
--->
<!--- POPULATE PDF FORM USING XML --->
<!---
<cfpdfform source="PdfFormExample.pdf" 
			destination="PdfFormExampleOut.pdf" 
			action="populate" 
			XMLdata="PdfFormExample.xml" 
			overwrite="yes" />
--->
