<!--- 
	CFPDF Action: removeWatermark
	Removes a watermark from specified pages of PDF
--->

<cfpdf action="removeWatermark" 
		source="P_and_L_WatermarkAdded.pdf" 
		pages="*" 
		destination="P_and_L_WatermarkRemoved.pdf" 
		overwrite="yes">
