<!--- 
	CFPDF Action: addWatermark
	Adds a watermark to specified pages of PDF
--->

<cfpdf action="addWatermark" 
		source="P_and_L_WithAttachments.pdf" 
		image="watermark.png" 
		destination="P_and_L_WatermarkAdded.pdf" 
		pages="*" 
		overwrite="yes">
