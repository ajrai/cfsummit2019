<!---
	CFPDF Action: MERGE
	2 ways to merge:
		1. Use directory
		2. Use cfpdfparam to merge individual PDFs
--->

<!--- 
	Use the cfpdf tag and cfpdfparam tags to merge individual PDF documents 
	into a new PDF document called new.pdf
--->
<cfpdf action="merge" 
		destination="P_and_L.pdf" 
		overwrite="yes" > 
	<cfpdfparam source="P_and_L_1.pdf"> 
	<cfpdfparam source="P_and_L_2.pdf"> 
</cfpdf>
