<!---
	CFPDF Action: MERGE
	2 ways to merge:
		1. Use directory
		2. Use cfpdfparam to merge individual PDFs
--->

<!--- merge all the pdf documents in a directory --->
<cfpdf action="merge" 
		directory="merge1" 
		order="name" 
		ascending="yes" 
		destination="output1.pdf" 
		overwrite="yes">
