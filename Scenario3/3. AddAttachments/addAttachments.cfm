<!---
	CFPDF Action: addAttachments
	1. Attach files to PDFs
	2. Reference and not a copy
	3. No EXE, VBS and ZIP
--->

<cfpdf action="addAttachments" 
 		source="#ExpandPath('P_and_L_merged.pdf')#" 
		destination="#ExpandPath('P_and_L_WithAttachments.pdf')#" 
		overwrite="true">
	
	<cfpdfparam source="#ExpandPath('Referrer_Redacted.pdf')#" 
				filename="referall.pdf" 
				description="This PDF to be referred for taking a look at what to redact" 
				encoding="UTF-8" />
	
</cfpdf>

