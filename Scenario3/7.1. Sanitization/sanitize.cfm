<!---
	CFPDF Action: sanitize
	1. The Sanitize action removes metadata from your PDF document so that 
	sensitive information is not inadvertently passed along when you publish your PDF
	2. Removes metadata, scripts, comments, form data, etc
--->

<cfpdf action="sanitize" 
		source="#ExpandPath('P_and_L_Redacted.pdf')#" 
		destination="#ExpandPath('P_and_L_Sanitized.pdf')#" 
		overwrite="true" />
