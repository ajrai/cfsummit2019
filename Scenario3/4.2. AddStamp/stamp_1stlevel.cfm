<!--- 
	CFPDF Action: addstamp
	1. Works similar to adding rubber stamp to paper
	2. Supported stamps:
		Approved, Experimental, NotApproved, AsIs
		Expired, NotForPublicRelease, Confidential
		Final, Sold, Departmental, ForComment, 
		TopSecret, Draft, ForPublicRelease
	3. Added in cfpdfparam as coordinates
--->

<cfpdf action="addstamp" 
 	source="#ExpandPath('P_and_L_WatermarkAdded.pdf')#"
 	destination="#ExpandPath('P_and_L_Stamped.pdf')#" 
 	overwrite="true">
	
	<cfpdfparam pages="1-2" coordinates="330, 158, 383, 178" 
		iconname="Approved" note="Approved at 1st level" />

</cfpdf>
