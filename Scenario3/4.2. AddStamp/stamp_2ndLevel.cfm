<!--- 
	CFPDF Action: addstamp
	1. Works similar to adding rubber stamp to paper
	2. Supported stamps:
		Approved, Experimental, NotApproved, AsIs
		Expired, NotForPublicRelease, Confidential
		Final, Sold, Departmental, ForComment, 
		TopSecret, Draft, ForPublicRelease
	3. Added in cfpdfparam as coordinates
--->

<cfpdf action="addstamp" 
	source="#ExpandPath('P_and_L_Stamped.pdf')#"
	destination="#ExpandPath('P_and_L_Stamped_Second.pdf')#" 
	overwrite="true">

	<cfpdfparam pages="2" coordinates="330, 118, 383, 138" 
		iconname="Final" note="Approved at 2nd level. No furthur changes to be made" />

</cfpdf>
