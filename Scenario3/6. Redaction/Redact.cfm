<!---
	CFPDF Action: Redact
	1. ColdFusion can keep PDF’s private information or content hidden. 
	2. Information can be redacted.
	3. You can redact an image or text in a PDF document
	4. One can do coordinate based redaction on specific pages of the PDF
--->

<cfpdf action="redact" source="#ExpandPath('P_and_L_Stamped_Third.pdf')#" 
   destination="#ExpandPath('P_and_L_Redacted.pdf')#" overwrite="true">
	<cfpdfparam pages="2" coordinates="380, 695, 410, 705" />
	<cfpdfparam pages="2" coordinates="465, 695, 495, 705" />
	<cfpdfparam pages="2" coordinates="380, 595, 410, 605" />
	<cfpdfparam pages="2" coordinates="465, 595, 495, 605" />
	<cfpdfparam pages="2" coordinates="380, 540, 410, 550" />
	<cfpdfparam pages="2" coordinates="460, 540, 495, 550" />
	<cfpdfparam pages="2" coordinates="380, 510, 410, 520" />
	<cfpdfparam pages="2" coordinates="460, 510, 495, 520" />
</cfpdf>
