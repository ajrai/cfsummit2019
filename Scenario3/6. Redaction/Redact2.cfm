<!---
	CFPDF Action: Redact
	1. Supports text based redaction
	2. Word Matching creteria supported are:
		a. MATCHPARTIALWORD_MARKWHOLEWORD
		b. MATCHPARTIALWORD_MARKPARTIALWORD
		c. MARKWHOLEWORD
--->

<cfscript>
	cfpdf(
		action="redact", 
		source="#ExpandPath('NoRedaction.pdf')#", 
		destination="#ExpandPath('RedactedText.pdf')#", 
		overwrite="true")
		{
			cfpdfparam(
				wordstoredact=["dummy", "print"], ignorecase=true, pages="*",
				wordmatchingcriteria="MATCHPARTIALWORD_MARKWHOLEWORD");
			/*cfpdfparam(
				wordstoredact=["print"], ignorecase=true, pages="*",
				wordmatchingcriteria="MATCHPARTIALWORD_MARKPARTIALWORD" );*/
		    /*cfpdfparam(
		    	wordstoredact=["print"], ignorecase=true, pages="*", 
		    	wordmatchingcriteria="MARKWHOLEWORD" );*/
		}
</cfscript>
