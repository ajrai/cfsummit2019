<!---
	Word to PDF
	1. Convert word files to PDF
	2. Uses cfdocument for it
	3. Openoffice must be installed
	4. You can start your openoffice process for accepting remote connection by:
	"soffice -nologo -nodefault -norestore -nofirststartwizard -headless 
	-accept="socket,host=IPADDRESS,port=8900;urp;StarOffice.ServiceManager""
--->

<cfdocument 
    format="pdf" 
    srcfile="#ExpandPath('MyDocument.doc')#" 
    filename="#ExpandPath('MyDocument.pdf')#"> 
</cfdocument>
