<!---
	CFPDF Action: protect
	Encrypt a PDF, or password protect it
--->

<cfpdf action="protect" 
		source="P_and_L_Final.pdf" 
		destination="P_and_L.pdf"  
		newUserPassword="password"
		overwrite="yes" >
