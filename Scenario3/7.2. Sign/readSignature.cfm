<!--- 
	CFPDF Action: readsignaturefields
	Read signature information from a PDF
--->

<cfpdf action="readsignaturefields" 
		source="#ExpandPath('P_and_L_Signed.pdf')#" 
		name="signinfo" 
		password="owner" />
		
<cfdump var="#signinfo#"/>
