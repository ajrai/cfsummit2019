<!--- 
	CFPDF Action: unsign
	Remove digital signature from an already signed PDF document
--->
<cfpdf action="unsign" 
		source="#ExpandPath('P_and_L_Signed.pdf')#" 
		destination="#ExpandPath('P_and_L_Unsigned.pdf')#" 
		unsignall="true" 
		overwrite="true">
		