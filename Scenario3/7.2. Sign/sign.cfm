<!--- 
	CFPDF Action: sign
	1. Digital signatures can be used in PDF documents to 
	authenticate the identity of a user and the document’s contents
	2. A signature contains information about the signer and state of the document when it was signed.
	3. It can be used to check whether the signed content was changed after the pdf was signed.
--->
<cfpdf action="sign" 
		source="#ExpandPath('P_and_L_Sanitized.pdf')#" 
		destination="#ExpandPath('P_and_L_Signed.pdf')#"
 		keystore="C:\ColdFusion2018\cfusion\wwwroot\Document Management\Scenario3\7.2. Sign\abcde.pfx" 
		keystorepassword="abcdef" 
		overwrite="true" 
		pages="2" 
		height="100" 
		width="100" 
		position="100,100" 
		author="false" />

