<!---
	CFPDF Action: archive
	1. You can archive pdf files. 
	2. PDF/A is an ISO-standardized version of the Portable Document Format (PDF) 
	specialized for the long-term digital preservation of electronic documents
	3. Supports standards PDF/A-1b/2b/3b
--->

<cfpdf action="archive" 
		source="#ExpandPath('P_and_L_Signed.pdf')#" 
		destination="#ExpandPath('P_and_L_Final.pdf')#" 
		overwrite="true" 
		standard="3b" />
		