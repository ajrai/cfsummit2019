<cfhtmltopdf>
	<cfhtmltopdfitem type="header">
		Page: _PAGENUMBER of _LASTPAGENUMBER 
	</cfhtmltopdfitem>
	
	<h2>Hi There</h2>
	<p>
		Its a test page to show how to use cfchart with cfhtmltopdf
		Also shows how to use images for footer 
	</p>
	
	<cfchart format="png" type="bar" showlegend="false" chartHeight="400" chartWidth="600" title="Basic Chart" show3d="true">
	    <cfchartseries>
	        <cfchartdata item="2015" value=20>
	        <cfchartdata item="2016" value=40>
	        <cfchartdata item="2017" value=60>
	    </cfchartseries>
	</cfchart>
	
	<cfhtmltopdfitem type="pagebreak" />
	<p>Use pagebreak to divide the PDF into pages</p> 
	
	<cfhtmltopdfitem type="footer" image="test.jpg">
	</cfhtmltopdfitem>
</cfhtmltopdf>
